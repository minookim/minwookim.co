---
title: "About"
date: 2018-06-22T18:29:50-04:00
draft: false
type: "home"
---

{{< imgproc home Resize "800x" >}}

**TL;DR: Born in Seoul, attended Columbia University, worked at Goldman Sachs,
and then joined Google as a software engineer, building tools to create
data-driven, dynamic advertisements.**

I am a Seoul-born NYC-based technologist, coffeephile, and gallerygoer.
I now work at Google as a software engineer.

I completed my degree in computer science at Columbia University.
Upon graduation in 2016, I worked in finance engineering at Goldman Sachs,
primarily building a tool to run realtime risk analysis on firmwide financial
products against various hypothetical market scenarios.

I joined Google in February 2018 and since then have been working as part of the
dynamic ads team, enabling advertisers to create programmatically generated,
personally tailored advertisements.

In the past, I worked on campus as the webmaster of Columbia Center on
Organizational Innovation and held summer internships at InterSystems
Corporation as a software developer and at BNP Paribas and Royal Bank of
Scotland as a research analyst.

When away from the keyboard, I like to go to art museums and classical concerts.
Still a dilettante, I have worked as a docent at a contemporary art gallery in
Seoul, providing guided tours of international contemporary art exhibitions.
